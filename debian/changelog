evidian (2025.1) unstable; urgency=medium

  * Updated Evidian to 1.5.8825

 -- Development Team <devteam@openthinclient.com>  Tue, 04 Feb 2025 22:47:05 +0100

evidian (2022.1.3) unstable; urgency=medium

  * Updated Evidian to 1.5.8705 (new support of RFIDeas reader)

 -- Development Team <devteam@openthinclient.com>  Thu, 30 Nov 2023 15:19:36 +0100

evidian (2022.1.2) unstable; urgency=medium

  * Added "Server timeout" option

 -- Development Team <devteam@openthinclient.com>  Fri, 04 Aug 2023 11:57:18 +0200

evidian (2022.1.1) unstable; urgency=medium

  * Updated Evidian to 1.5.8273. This fixes:
    - Error message "Authentication failed" after password change with Smartcard
    - Message when trying to enroll a second badge is not allowed.

 -- Development Team <devteam@openthinclient.com>  Mon, 27 Feb 2023 17:00:18 +0200

evidian (2022.1) unstable; urgency=medium

  * Version bump

 -- Development Team <devteam@openthinclient.com>  Wed, Sep 14 2022 16:33:34 +0200

evidian (2021.2.1) unstable; urgency=medium

  * Updated Evidian to 1.5.7973
  * Fixed issues with missing welcome dialog.

 -- Development Team <devteam@openthinclient.com>  Mon, 13 Dec 15:34:57 +0200

evidian (2021.2) unstable; urgency=medium

  * Updated Evidian to 1.5.7883

 -- Development Team <devteam@openthinclient.com>  Fri, 17 Sep 2021 14:23:00 +0200

evidian (2020.2.1) unstable; urgency=medium

  * Update to 1.5.7617
  * Add new credentials handover to Citrix and Freerdp (incompatible with 2020.2 releases)

 -- Development Team <devteam@openthinclient.com>  Thu, 14 Jan 2021 13:43:51 +0200

evidian (2020.2) unstable; urgency=medium

  * Updated Evidian to 7313 (64Bit)

 -- Development Team <devteam@openthinclient.com>  Thu, 05 Nov 2020 12:51:20 +0200

evidian (2019.1.2) unstable; urgency=low

  * Fixed missing libraries

 -- Development Team <devteam@openthinclient.com>  Mon, 28 Oct 2019 11:53:11 +0200

evidian (2019.1.1) unstable; urgency=low

  * Fixed option hideWelcomeMessage

 -- Development Team <devteam@openthinclient.com>  Wed, 21 Oct 2019 12:00:01 +0200

evidian (2019.1) unstable; urgency=low

  * Added options passwordForgotten and hideWelcomeMessage
  * Updated rsUserAuth binary to 1.5.6840

 -- Development Team <devteam@openthinclient.com>  Wed, 31 Jul 2019 14:23:29 +0200

evidian (2018.1) unstable; urgency=low

  * Support of REST API was added

 -- Vladyslav Savchenko <v.savchenko@openthinclient.com>  Mon, 10 Sep 2018 16:00:00 +0200

evidian (2.1-1.4.6132-2) unstable; urgency=medium

  * added support for citrix storebrowse

 -- Marton Morvai <m.morvai@openthinclient.com>  Wed, 29 Aug 2018 13:50:34 +0200

evidian (2.1-1.4.6132-1) unstable; urgency=medium

  * initial release

 -- Marton Morvai <m.morvai@openthinclient.com>  Tue, 05 Jun 2018 12:41:22 +0200
