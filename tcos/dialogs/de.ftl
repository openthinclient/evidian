freerdp_started = FreeRDP startet
freerdp_stopped = FreeRDP gestoppt
citrix_started = Citrix startet
citrix_stopped = Citrix gestoppt

config-error-title = Evidian Konfigurationsfehler

config-error-missing-url-title = Keine Web-Service-URL für Evidian angegeben
config-error-missing-url-message =
    Evidian Auth Manager Client kann ohne eine User Access Web Service URL nicht funktionieren.

    Bitte wenden Sie sich an Ihren Administrator.

config-error-missing-app-title = Keine Anwendung für Evidian angegeben
config-error-missing-app-message =
    Evidian Auth Manager Client erfordert die Angabe einer Anwendung.

    Bitte wenden Sie sich an Ihren Administrator.

config-error-app-not-found-title = Anwendung für Evidian nicht gefunden
config-error-app-not-found-message =
    Die Anwendung "{ $app_name }" wurde nicht gefunden.

    Bitte wenden Sie sich an Ihren Administrator.

config-error-wrong-app-type = Falscher Anwendungstyp für Evidian
config-error-wrong-app-type-message =
    Die Anwendung "{ $app_name }" ({ $app_type }) ist nicht kompatibel mit Evidian Auth Manager Client.

    Es werden nur Anwendungen vom Typ FreeRDP und Citrix unterstützt.

    Bitte wenden Sie sich an Ihren Administrator.

error-title = Evidian-Fehler

error-uncaught = Ein unerwarteter Fehler ist aufgetreten.

    { $error }

    Bitte informieren Sie Ihren Administrator.
