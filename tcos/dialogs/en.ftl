freerdp_started = FreeRDP starting
freerdp_stopped = FreeRDP stopped
citrix_started = Citrix starting
citrix_stopped = Citrix stopped

config-error-title = Evidian configuration error

config-error-missing-url-title = No web service URL specified for Evidian
config-error-missing-url-message =
    Evidian Auth Manager Client cannot function without a User Access web service URL.

    Please contact your administrator.

config-error-missing-app-title = No Application specified for Evidian
config-error-missing-app-message =
    Evidian Auth Manager Client requires an application to be specified.

    Please contact your administrator.

config-error-app-not-found-title = Application not found for Evidian
config-error-app-not-found-message =
    The application "{ $app_name }" was not found.

    Please contact your administrator.

config-error-wrong-app-type = Wrong application type for Evidian
config-error-wrong-app-type-message =
    The application "{ $app_name }" ({ $app_type }) is not compatible with Evidian Auth Manager Client.

    Only FreeRDP and Citrix applications are supported.

    Please contact your administrator.

error-title = Evidian error

error-uncaught = An unexpected error occurred.

    { $error }

    Please inform your administrator.
